import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { QuotesContainerComponent } from './quotes/quotes-container.component';
import { QuotesCardComponent } from './quotes/quotes-card.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { SortContainerComponent } from './sort-by/sort-container.component';
import { SortByComponent } from './sort-by/sort-by.component';

@NgModule({
  declarations: [
    AppComponent,
    QuotesContainerComponent,
    QuotesCardComponent,
    SearchBarComponent,
    SortContainerComponent,
    SortByComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
