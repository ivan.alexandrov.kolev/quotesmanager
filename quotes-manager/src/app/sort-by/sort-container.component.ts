import { Component, OnInit } from '@angular/core';
import { QuotesManagerService } from '../services/quotes-manager.service';
import { SortCriteria, SortType, ISortOptionsData, ISortByData } from '../quotes-manager.models';

@Component({
  selector: 'sort-container',
  templateUrl: './sort-container.component.html',
})
export class SortContainerComponent implements OnInit {
    initialSortOptionsData: ISortOptionsData[];
    private currentSortByData: ISortByData[] = [];

    constructor(private quotesManagerService: QuotesManagerService){}    
    
    ngOnInit(): void {
        this.initialSortOptionsData = this.getInitialSortOptionsData();
        this.currentSortByData = this.initialSortOptionsData.filter(d => d.active).map(d => d.sortByData);
        this.requestSort();
    }

    resetFilter() {
        this.initialSortOptionsData = this.getInitialSortOptionsData(false);
        this.currentSortByData = [];
        // fake reordering by start property
        const sortByStart = [<ISortByData>{
            sortCriteria: SortCriteria.Start, 
            sortType: SortType.ASC
        }];
        this.requestSort(sortByStart);
    }

    onFilterClicked(sortByData: ISortByData): void   {
        const existingIndex = this.currentSortByData.findIndex(d => d.sortCriteria === sortByData.sortCriteria);
        
        if(existingIndex > -1){
            this.currentSortByData[existingIndex].sortType = sortByData.sortType 
        } else {
            this.currentSortByData.push(sortByData);
        }

        this.requestSort();
    }

    requestSort(sortByData?: ISortByData[]): void {
        this.quotesManagerService.sortQuotes(sortByData ? sortByData : this.currentSortByData);
    }

    private getInitialSortOptionsData(isNatSortActive = true): ISortOptionsData[] {
        const resultArr = [
            <ISortOptionsData> {
                active: isNatSortActive,
                title: "Participant nationality",
                sortByData: <ISortByData>{
                    sortCriteria: SortCriteria.ParticipantNationality,
                    sortType: SortType.ASC
                }
            },
            <ISortOptionsData> {
                active: false,
                title: "Participant name",
                sortByData: <ISortByData>{
                    sortCriteria: SortCriteria.ParticipantName,
                    sortType: SortType.ASC
                }
            }
        ];

        return resultArr;
    }
}
