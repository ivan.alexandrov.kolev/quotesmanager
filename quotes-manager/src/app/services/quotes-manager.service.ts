import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, Subject, BehaviorSubject } from 'rxjs';
import { IQuoteData, SortCriteria, SortType, ISortByData } from '../quotes-manager.models';
import { quotesConstants } from '../quotes.constants';
import { orderBy } from 'lodash';

@Injectable({
    providedIn: 'root'
})

export class QuotesManagerService {
    private _quotesData$ = new BehaviorSubject<IQuoteData[]>([]);
    quotesData$ = this._quotesData$.asObservable();
    
    quotesData: IQuoteData[];
    sortOptionsData: ISortByData[]
    activeSearchValue = '';
    constructor(private httpClient: HttpClient){}

    loadQuotesData(): void { 
        const dataUrl = quotesConstants.JSON_SERVER_BASE_URL.concat(quotesConstants.QUOTES_DATA_URL);

        this.httpClient.get<IQuoteData[]>(dataUrl)
            .subscribe(
                (data) => {
                    this.quotesData = data;
 
                    this.sortOptionsData ? this.sortQuotes(this.sortOptionsData) : this.emitQuotesData();
                },
                (error) => console.log("Failed loading quotes with error: ".concat(error))
            )
    }

    searchQuotes(searchValue: string): void {
        this.activeSearchValue = searchValue.toLowerCase();

        if(!this.quotesData) {
            return;
        }

        const filteredQuotes = this.quotesData.filter(q => q.content.toLowerCase().includes(this.activeSearchValue));

        this.emitQuotesData(filteredQuotes);
    }

    deleteQuote(quoteId: string): void {
        if(!this.quotesData) {
            return;
        }

        this.quotesData = this.quotesData.filter(q => q.id !== quoteId);
        this.activeSearchValue.length ? 
            this.searchQuotes(this.activeSearchValue) : 
            this.emitQuotesData();
    }

    sortQuotes(sortOptionsData: ISortByData[]): void {
        this.sortOptionsData = sortOptionsData;

        if(!this.quotesData) {
            return;
        }
        const sortCriterias = sortOptionsData.map(d => d.sortCriteria);
        const sortTypes = sortOptionsData.map(d => d.sortType);
        
        this.quotesData = orderBy(this.quotesData, sortCriterias, sortTypes)

        this.activeSearchValue.length ? 
            this.searchQuotes(this.activeSearchValue) : 
            this.emitQuotesData();
    }

    private emitQuotesData(filteredQutesData?: IQuoteData[]): void{
        const quoteDataToEmit = filteredQutesData ? filteredQutesData : this.quotesData;
        this._quotesData$.next(quoteDataToEmit);
    }
}