import { Component } from '@angular/core';
import { QuotesManagerService } from '../services/quotes-manager.service';

@Component({
  selector: 'search-bar',
  templateUrl: './search-bar.component.html',
})
export class SearchBarComponent {
    private searchString = '';

    constructor(private quotesManagerService: QuotesManagerService){}

    onSearch(event: any): void {
        this.searchString = event.target.value;

        this.quotesManagerService.searchQuotes(this.searchString);
    }
}
