import { Component, OnInit } from '@angular/core';
import { QuotesManagerService } from '../services/quotes-manager.service';
import { IQuoteData } from '../quotes-manager.models';
import { Subscription, fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators'; 

@Component({
  selector: 'quotes-container',
  templateUrl: './quotes-container.component.html',
})
export class QuotesContainerComponent implements OnInit {
    data: IQuoteData[]; 
    quotesColumnsData: IQuoteData[][];

    private quotesSubscription = new Subscription();
    private windowSizeSubscription = new Subscription();

    constructor(private quotesManagerService: QuotesManagerService){
        this.quotesManagerService.loadQuotesData(); 
    }

    ngOnInit(): void {
        this.quotesSubscription = this.quotesManagerService.quotesData$.subscribe(data => {
            this.data = data;
            this.calculateColumns(this.isSingleColumn());
        });

        this.windowSizeSubscription = fromEvent(window, 'resize').pipe(
            debounceTime(50),
        ).subscribe(() => {
            this.calculateColumns(this.isSingleColumn())
        });
    }

    private isSingleColumn(): boolean {
        return window.innerWidth <= 720;
    }

    calculateColumns(isSingleColumn: boolean) {
        this.quotesColumnsData = [];

        if(isSingleColumn) {
            this.quotesColumnsData[0] = this.data;
            return;
        } 

        this.quotesColumnsData = [[], []];

        this.data.map((quote, index) => {
            index % 2 === 0 ? 
                this.quotesColumnsData[0].push(quote) : 
                this.quotesColumnsData[1].push(quote);
        });
    }

    ngOnDestroy(): void {
        this.quotesSubscription.unsubscribe();
        this.windowSizeSubscription.unsubscribe();
    }

    onXClicked(quoteId: string): void {
        this.quotesManagerService.deleteQuote(quoteId);
    }

    trackByFn(quoteData: IQuoteData): string {
        return `${quoteData.id}`;
    }
}
