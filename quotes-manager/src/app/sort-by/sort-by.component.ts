import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {  SortType, ISortOptionsData, ISortByData } from '../quotes-manager.models';

@Component({
  selector: 'sort-by',
  templateUrl: './sort-by.component.html',
})
export class SortByComponent implements OnInit {
    isActive = false;
    isAscending = true;

    private currentSortByData: ISortByData;
    @Input() sortOptions: ISortOptionsData;

    @Output() sortClicked = new EventEmitter<ISortByData>();

    ngOnInit():void {
        this.isActive = this.sortOptions.active;
        this.currentSortByData = this.sortOptions.sortByData;
    }

    onClick() {
        if(this.isActive) {
            if(this.currentSortByData.sortType === SortType.ASC) {
                this.isAscending = false;
                this.currentSortByData.sortType = SortType.DESC; 
            } else {
                this.isAscending = true;
                this.currentSortByData.sortType = SortType.ASC;
            }
        } else {
            this.isActive = !this.isActive; // disabled -> turnedOn
        }

        this.sortClicked.emit(this.currentSortByData);
    }
}
