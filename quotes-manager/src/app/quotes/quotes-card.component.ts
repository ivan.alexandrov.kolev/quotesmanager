import { Component, Input, Output, EventEmitter } from '@angular/core';
import { QuotesManagerService } from '../services/quotes-manager.service';
import { IQuoteData } from '../quotes-manager.models';

@Component({
  selector: 'quotes-card',
  templateUrl: './quotes-card.component.html',
})
export class QuotesCardComponent {
    @Input() quoteData: IQuoteData;

    @Output() xClicked = new EventEmitter<void>();

    constructor(private quotesManagerService: QuotesManagerService){
    }

    onXClicked(): void {
        this.xClicked.emit();
    }
}
