export interface IQuoteData {
    id: string,
    content: string,
    tags: string[],
    participant: {
        age: number,
        name: string,
        nationality: string
    },
    start: number
}
export interface ISortOptionsData {
    active: boolean,
    title: string,
    sortByData: ISortByData
}
export enum SortType {
    ASC = 'asc',
    DESC = 'desc'
}
export enum SortCriteria {
    ParticipantNationality = "participant.nationality",
    ParticipantName = "participant.name",
    Start = "start"
}

export interface ISortByData {
    sortCriteria: SortCriteria,
    sortType: SortType
}
